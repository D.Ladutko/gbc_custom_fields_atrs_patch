# -*- coding: utf-8 -*-

import datetime
import dateutil.relativedelta as relativedelta
import logging
from lxml import etree
from babel.dates import format_date
import functools

from odoo import api, models, _
from odoo.osv import expression
from odoo.tools.safe_eval import safe_eval
from odoo.osv import expression

_logger = logging.getLogger(__name__)


class Base(models.AbstractModel):
    _inherit = 'base'

    def customize_button(self, button):
        # correction for timesheet button
        if self._name == 'project.project' and button.get('attrs').get('name') == 'action_show_timesheets_by_employee_invoice_type':
            button['attrs']['type'] = 'action'
            button['attrs']['name'] = 'hr_timesheet.timesheet_action_all'
            button['gbc_values'] = {
                'action_context': {'default_project_id': self.id,},
                'action_domain': [('project_id', '=', self.id)],
                'action_name': _('{} Timesheets').format(self.name),
            }
        return button

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        has_access = functools.partial(self.check_access_rights, raise_exception=False)
        readonly = not (has_access('write') or has_access('create'))

        res = super(Base, self).fields_get(allfields, attributes)
        for fname, description in res.items():
            field = self._fields[fname]
            if hasattr(field, 'gbc_hide') and field.gbc_hide:
                description['searchable'] = False
            if readonly:
                description['readonly'] = True
                description['states'] = {}
            if attributes:
                description = {
                    key: val for key, val in description.items() if key in attributes
                }
            res[fname] = description

        return res

    def get_button_action(self, button):
        attrs = button.get('attrs')
        button_type = attrs.get('type')
        action_func = getattr(self, attrs.get("name"), None)
        if button_type == 'object' and callable(action_func):
            return action_func()
        if button_type == 'action':
            action_xml = attrs.get('name')
            if not action_xml.isdigit():
                return self.env.ref(action_xml).sudo().read()[0]
            action = self.env['ir.actions.act_window'].browse(int(action_xml)).sudo().read()[0]
            if attrs.get('context'):
                action['context'] = attrs.get('context')
            return action

        return {}

    def prepare_action(self, action, eval_context):
        domain = action.get('domain', [])
        context = action.get('context', {})
        model = action.get('res_model')
        search_view_id = action.get('search_view_id')

        context = safe_eval(context, eval_context) if isinstance(context, str) else context
        domain = safe_eval(domain, eval_context) if isinstance(domain, str) else domain
        context_domain = self.get_domain_from_context(context, model, search_view_id) if context else [[]]
        context_domain.append(domain)

        context = dict(context)
        context['active_id'] = self.id
        action['context'] = context.copy()
        action['domain'] = domain

        return expression.AND([r for r in context_domain if r])

    def get_title(self, action, attrs, rec_ids):
        title = action.get('name', action.get('res_model'))
        if attrs.get('name') == 'action_show_potential_duplicates':
            rec_ids = rec_ids.filtered(lambda r: r.id != self.id)
            title = _('Similar Leads')
        elif attrs.get('name') == 'action_dependent_tasks':
            title = _('Dependent Tasks')

        return title

    def get_button_box_relations(self, buttons):
        res = []
        if len(self) != 1:
            return res

        config_obj = self.env['ir.config_parameter'].sudo()
        info_card_records_limit = int(config_obj.get_param('gbc_web_theme.infocard_records_limit', 5))
        eval_context = self._gbc_get_eval_context()

        for i, button in enumerate(buttons or [], start=1):
            button = self.customize_button(button)
            action = self.get_button_action(button)
            # exclude Burndown Chart

            model_name = action.get('res_model')
            if model_name:
                if not self.env[model_name].check_access_rights('read', raise_exception=False):
                    continue

            if action.get('type') != 'ir.actions.act_window' or 'report' in action.get('res_model'):
                continue
            if action.get('res_model') == 'helpdesk.ticket':
                action['name'] = _('Helpdesk tickets')
                action['views'] = [[False, 'list'], [False, 'kanban'], [False, 'form'], [False, 'pivot']]
            # exclude "hr_timesheet.timesheet_action_all" action repeat
            btn_attrs = button.get('attrs')
            if btn_attrs and btn_attrs.get('type') == 'object' and btn_attrs.get('name') == 'action_billable_time_button':
                continue

            domain = self.prepare_action(action, eval_context)
            if button.get('gbc_values'):
                domain = expression.AND([
                    button.get('gbc_values').get('action_domain') or [],
                    domain or [],
                ])
            model = action.get('res_model')
            rec_count = self.env[model].search_count(domain or [])
            rec_ids = self.env[model].search(domain or [], limit=info_card_records_limit)

            _fields_obj = self.env[model].fields_get()
            title = self.get_title(action, button.get('attrs'), rec_ids)

            if action.get('view_mode') == 'form' or action.get('views') == [[False, "form"]]:
                rec_ids = self.env[action.get('res_model')].browse(action.get('res_id'))
                title = action.get('name', rec_ids.display_name)
                if not action.get('views'):
                    action['views'] = [[False, 'form']]

            if button.get('gbc_values'):
                action['context'] = button.get('gbc_values').get('action_context')
                action['domain'] = button.get('gbc_values').get('action_domain')
                action['name'] = action['display_name'] = button.get('gbc_values').get('action_name')

            if btn_attrs.get('context'):
                try:
                    add_context = safe_eval(btn_attrs.get('context'), eval_context)
                    action['context'] = dict(list(action['context'].items()) + list(add_context.items()))
                except Exception as e:
                    pass
            # Exception for applicant card events
            if btn_attrs.get('name') == 'action_makeMeeting' and self._name == 'hr.applicant':
                rec_ids = self.meeting_ids
                rec_count = len(self.meeting_ids)
                action['domain'] = [('applicant_id', '=', self.id)]

            item = {
                'order': i,
                'title': title,
                'data': rec_ids.read(_fields_obj.keys()),
                'action': action,
                'count': rec_count,
                'view': 'table' if model != 'calendar.event' else 'list',
                'fields_map': {
                    r: [_fields_obj.get(r).get('string'), _fields_obj.get(r).get('type')] for r in _fields_obj
                }
            }
            res.append(item)

        return res

    @api.model
    def get_domain_from_context(self, context, model, search_view_id):
        domains = [[]]
        eval_context = self._gbc_get_eval_context()
        search_domains = self.get_search_domains(model, search_view_id)
        model_fields = set(self.env[model].fields_get().keys())

        for key, value in context.items():
            if key.startswith('search_default_') and not key.startswith('search_default_groupby_'):
                search_key = key.replace('search_default_', '')
                if search_domains.get(search_key):
                    domains.append(safe_eval(search_domains[search_key], eval_context))
                elif search_key in model_fields:
                    domains.append(safe_eval(f'[("{search_key}", "=", {value})]', eval_context))

        return domains

    @api.model
    def get_search_domains(self, model, search_view_id):
        options = {'load_filters': True}
        search_form_id = search_view_id[0] if search_view_id else False
        views = self.env[model].load_views([[search_form_id, 'search']], options)
        search_view = views.get('fields_views').get('search')
        # === Get filters from search view ===
        tree = etree.fromstring(search_view.get('arch'))
        filter_tags = tree.xpath("//filter")
        # === Get filters domains by filter name ===
        search_domains = {}
        for f in filter_tags:
            if f.attrib.get('name') and f.attrib.get('domain'):
                search_domains[f.attrib.get('name')] = f.attrib.get('domain')
        return search_domains

    # === Evaluation context for compute variables in domain ===
    def _gbc_get_eval_context(self):
        return {
            'user': self.env.user.id,
            'user_id': self.env.user.id,
            'uid': self.env.user.id,
            'partner_id': self.env.user.partner_id.id,
            'company_ids': self.env.companies.ids,
            'allowed_company_ids': self.env.companies.ids,
            'company_id': self.env.company.id,
            'active_id': self.id,
            'datetime': datetime.datetime,
            'context_today': datetime.datetime.now,
            'relativedelta': relativedelta.relativedelta,
            'id': self.id,
            'project_id': self.id,
            'name': self.display_name,
            'employee_id': self.env.user.employee_id.id,
            'employee_ids': self.env.user.employee_ids.ids,
        }

    # the two methods below are defined for adding a parent task to the kanban board and overriding the name
    def gbc_action_open_project_task(self):
        """
        method returns the action to display the parent task
        :return: dictionary
        """
        parent_task = self.parent_id.id
        if parent_task:
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'project.task',
                'res_id': parent_task,
                'view_mode': 'form',
                'target': 'current',
            }

    def open_activities_calendar(self):
        return {
            'name': f'Activities for {self.name}',
            'type': 'ir.actions.act_window',
            'res_model': 'mail.activity',
            'view_mode': 'calendar',
            'views': [[self.env.ref('gbc_activity.mail_activity_view_calendar').id, 'calendar']],
            'domain': [('id', 'in', self.activity_ids.ids)],
            'target': 'current',
        }

    # This method needs for kanban timesheets
    @api.model
    def web_read_group(self, domain, fields, groupby, limit=None, offset=0, orderby=False,
                       lazy=True, expand=False, expand_limit=None, expand_orderby=False):

        # === PATCH for timesheets kanban (the context is setted from the js kanban model)  ===
        if self.env.context.get('isTimesheetKanban'):
            weekStart = self.env.context.get('isTimesheetKanbanWeekStart')
            workDays = self.env.context.get('isTimesheetKanbanWorkDays')
            # getting week dates, reading data-groups for dates
            dates = []
            groups = []
            # today = datetime.date.today()
            # monday = today - datetime.timedelta(days=today.weekday())
            monday = datetime.date.fromisoformat(weekStart)
            for i in range(workDays):
                # save dates
                d = monday + datetime.timedelta(days=i)
                dates.append(d)
                # get group
                dom = expression.AND([domain, [('date', '=', d.isoformat())]])
                gb = ['date:day']
                groups.append(
                    super(Base, self).web_read_group(
                        domain=dom, fields=fields, groupby=gb, limit=limit, offset=offset, orderby=orderby, lazy=False,
                        expand=expand, expand_limit=expand_limit, expand_orderby=expand_orderby)
                )
            # add empty groups for empty-data dates
            res = {
                'groups': [],
                'length': 0,
            }
            i = 0
            for group in groups:
                data = group.get('groups')
                locale = self.env.user.lang or 'en_GB'
                if locale == 'en_US':
                    locale = 'en_GB'
                if not len(data):
                    d1 = dates[i]
                    d2 = datetime.date.strftime(d1 + datetime.timedelta(days=1), '%Y-%m-%d')
                    data = [{
                        'date_count': 0,
                        'unit_amount': 0.0,
                        'date:day': format_date(dates[i], locale=locale).replace(',', ''),
                        '__range': {'date': {'from': d1, 'to': d2}}
                    }]
                res['groups'] += data
                i += 1
            res['length'] = len(res['groups'])
            return res

        return super(Base, self).web_read_group(domain, fields, groupby, limit, offset, orderby, lazy, expand, expand_limit, expand_orderby)
