# -*- coding: utf-8 -*-

from . import models
from . import controllers
from . import monkey_patch
from . import Gbcattr

from odoo import api, SUPERUSER_ID
from .hooks import start

def _uninstall_reset_changes(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    env['web_editor.assets'].reset_asset(
        '/gbc_web_theme/static/src/colors.scss',
        'web._assets_primary_variables'
    )

def _upgrade_hook(cr, registry):
    start(cr, registry)