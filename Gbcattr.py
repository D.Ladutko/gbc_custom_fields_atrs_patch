from odoo import fields

# Сохраняем оригинальные реализации методов
original_field_init = fields.Field.__init__
original_get_description = fields.Field.get_description


# Переопределяем инициализатор класса Field
def new_field_init(self, *args, **kwargs):
    original_field_init(self, *args, **kwargs)
    self.gbc_hide = kwargs.get('gbc_hide', False)


fields.Field.__init__ = new_field_init


# Переопределяем метод get_description класса Field
def new_get_description(self, env):
    description = original_get_description(self, env)
    description['gbc_hide'] = getattr(self, 'gbc_hide', False)
    return description


fields.Field.get_description = new_get_description
